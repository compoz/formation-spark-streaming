package com.fabulouslab.spark.e01_basic

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.types.{BooleanType, StringType, StructField, StructType}

object E03_parquet_stream {

  case class Video( video_id: String, title: String, channel_title : String, category_id : Int, tags: String, views : Int , likes : Long, dislikes :Int, comment_total :Int, thumbnail_link : String, date : String)

  def main(args: Array[String]) {

    /**
     *   Ici on veut:
     *   - Charger en mode stream un fichier parquet (src/main/resources/videos.parquet)
     *   - Transformer le dataframe en dataset de Video (utiliser la définition de la classe déclarée dans la ligne 9)
     *   - Créer un deuxième flux en filtrant le premier flux avec un like > 1000, et register ce stream  dans une table sous format parquet
     *   - Créer un troisième flux en filtrant le premier flux avec un like < 1000, et register ce stream  dans une table sous format parquet
     *   - Créer un quatrième flux en  comptant le nombre de vidéo qui n'ont aucun commentaire, et register ce stream  dans une table sous format parquet
     * */


  }

}