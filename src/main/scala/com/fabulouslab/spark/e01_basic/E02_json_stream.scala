package com.fabulouslab.spark.e01_basic

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.types.{BooleanType, IntegerType, StringType, StructField, StructType}

object E02_json_stream {

  def main(args: Array[String]) {

    /**
      *   Ici on veut:
      *   - Charger en mode stream un fichier json (src/main/resources/json) avec le schéma approprié.
      *   - Enregister le stream dans une table, et via sql compter le nombre de catégorie
      *   - Écrire le stream en mémoire
      *   - se connecter sur Spark UI pour monitorer le stream
      * */

  }

}
