package com.fabulouslab.spark.e01_basic

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.types.StructType

object E01_csv_stream {

   def main(args: Array[String]) {

     /**
      *    Le but est de faire une comparaison entre des chargements des fichiers en mode batch vs en mode stream:
       *   et voila les étapes pour y parvenir :
       *   - En utilisant l'api spark batch charger le ficher csv src/main/resources/csv/GBcomments.csv.
       *   - Afficher son schéma.
       *   - Refaire le chargement avec l'api Spark streaming, et écrire le résultat dans la console ? Pour quoi il y'a une erreur ?
       *   - Refaire le chargement en adaptant le chemin et en fournissant un schéma.
       *   - Dupliquer le fichier, comment Spark va se comporter en mode stream ?
       *   Hint : pour réduire la verbosité des logs fixer le niveau de log a error:      sparkSession.sparkContext.setLogLevel("ERROR")
      */
   }

}
